# pkg --public -t node16-macos-x64 --out-path dist/macos index.js 
# pkg --config build.json --public -t node16-macos-x64 --out-path dist/macos index.js 
# pkg --config build.json --public -t node16-macos-x64 --out-path dist/autoscripts-macos setup.js 
rm -Rf ./deploy/win32/build

npx nodegui-packer --init autoscripts

npm run build

npx nodegui-packer --pack ./dist

node build.js

cp -Rf ../autoscripts/dist/win/* ./deploy/win32/build/autoscripts/

chmod -Rf 755 ./deploy/win32/build/autoscripts/index.exe

chmod -Rf 755 ./deploy/win32/build/autoscripts/chrome-win