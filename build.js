const rcedit = require('rcedit')
const fs = require('fs')
const path = require('path')


  ; (async () => {
    const exePath = path.resolve(
      process.cwd(),
      'deploy/win32/build/autoscripts/qode.exe'
    )
    const icoPath = path.resolve(
      process.cwd(),
      'ico/favicon256.ico'
    )
    // console.log(exePath);
    // console.log(icoPath);

    await rcedit(exePath, { icon: icoPath })


    await fs.renameSync(exePath,
      path.resolve(path.dirname(exePath), '一键启动.exe'));

    // await fs.fchmodSync(path.resolve(path.dirname(exePath), 'index.exe'),0o775)

  })()