import { QGroupBox, QWidget, QLabel, QComboBox, FlexLayout, QPushButton, QLineEdit, QPixmap, QCheckBox, QRadioButton } from '@nodegui/nodegui';


let createImage = function (img, w = 0, h = 0, className = '') {
  let image = new QPixmap();
  image.load(img);
  if (w && h) {
    image = image.scaled(w, h, 1)
  }
  const labelLogo = new QLabel();
  labelLogo.setPixmap(image);
  if (className) {
    labelLogo.setObjectName(className);
  }
  return labelLogo
}

let createLabel = function (text, className = '') {
  const label = new QLabel();
  if (className) {
    label.setObjectName(className);
  }
  label.setText(text);
  return label
}

let createCheckboxList = function (mList, className = '') {
  for (const m of mList) {
    const r1 = new QCheckBox();
    r1.setText(m.v);
    m['o'] = r1
  }

  const groupBoxLayout = new FlexLayout();
  const groupBox = new QGroupBox();
  if (className) {
    groupBox.setObjectName(className);
  }
  groupBox.setLayout(groupBoxLayout);
  for (const r of mList) {
    groupBoxLayout.addWidget(r.o);
  }
  return groupBox
}

let createRadioList = function (mList, className = '') {

  for (const m of mList) {
    const r1 = new QRadioButton();

    r1.setText(m.v);
    m['o'] = r1
  }

  const groupBoxLayout = new FlexLayout();
  const groupBox = new QGroupBox();
  if (className) {
    groupBox.setObjectName(className);
  }
  groupBox.setLayout(groupBoxLayout);
  for (const r of mList) {
    groupBoxLayout.addWidget(r.o);
  }
  return groupBox
}


function createSearchContainer(onSearch) {
  const comboBox = new QComboBox();
  // comboBox.addItem(undefined, 'comboBox item 0');

  comboBox.addEventListener('currentTextChanged', (text) => {
    console.log('currentTextChanged: ' + text);
    searchInput.setText(text)
  });

  const searchContainer = new QWidget();
  searchContainer.setObjectName('searchContainer');
  searchContainer.setLayout(new FlexLayout());

  const searchInput = new QLineEdit();
  searchInput.setObjectName('searchInput');

  searchInput.addEventListener('textChanged', () => {
    onSearch(searchInput.text(), comboBox);
  });


  const accountLabel = new QLabel();
  accountLabel.setText('钱包地址：')
  accountLabel.setObjectName('accountLabel');

  searchContainer.layout?.addWidget(accountLabel);
  searchContainer.layout?.addWidget(searchInput);
  searchContainer.layout?.addWidget(comboBox);

  searchContainer.setStyleSheet(`
    #searchContainer {
      flex-direction: 'row';
      padding-top: 5px;
    }
    #searchInput {
      flex: 1;
    }
  `);
  return searchContainer;
}

export { createImage, createLabel, createCheckboxList, createSearchContainer, createRadioList }
